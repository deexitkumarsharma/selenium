package com.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebElements {
    public static void main(String[] args) {
        // open the Chrome browser
        System.setProperty("webdriver.chrome.browser", "/home/deexit/Downloads/Selenium/drivers");
        WebDriver driver = new ChromeDriver();

        // max the Chrome browser
        driver.manage().window().maximize();

        //Navigate to the URL
        driver.get("https://www.flipkart.com");

        //[By Id]
//        driver.findElement(By.id("search_query_top")).sendKeys("T-Shirts");
        //OR
        WebElement search = driver.findElement(By.id("search_query_top"));
        search.sendKeys("T-shirts");

        //[By Name]
        driver.findElement(By.name("search_query")).sendKeys("Shirts");

        //[By LinkText]
//        driver.findElement(By.linkText("Printed Chiffon Dress")).click();

        //[By Partial LinkText]
        driver.findElement(By.partialLinkText("Chiffon Dress")).click();

    }

}
