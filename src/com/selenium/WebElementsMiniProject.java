package com.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

// 1. Open page
// 2. Type username student into Username field
// 3. Type password Password123 into Password field
// 4. click Submit button
// 5. Verify new page URL contains practicetestautomation.com/logged-in-successfully/
// 6. Verify new page Title
// 7. Display the page source code
// 8. Close the Browser

public class WebElementsMiniProject {
    public static void main(String[] args) {
        // open the Chrome browser
        System.setProperty("webdriver.firefox.browser", "/home/deexit/Downloads/Selenium/drivers/firefoxdriver");
//        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();

        // max the Chrome browser
        driver.manage().window().maximize();

        //Navigate to the URL
        driver.get("https://practicetestautomation.com/practice-test-login/");

        //Enter valid username
        driver.findElement(By.id("username")).sendKeys("student");

        //Enter valid password
        driver.findElement(By.name("password")).sendKeys("Password123");

        //Click on submit
        driver.findElement(By.id("submit")).click();

        //Verify the URL
        String act_url = driver.getCurrentUrl();
//        System.out.println(act_url);
        //for verify
        String expected_url = "https://practicetestautomation.com/logged-in-successfully/";
        if (act_url.equals(expected_url)) {
            System.out.println("URL Matched Successfully");
        } else {
            System.out.println("URL Matched Failed");
        }

        //Verify Title
        //getTitle()
        String act_Title = driver.getTitle();
//        System.out.println(act_Title);
        String expected_title = "Logged In Successfully | Practice Test Automation";
        ///verify
        if (act_Title.equals(expected_title)) {
            System.out.println("Title Matched Successfully");
        } else {
            System.out.println("Title Matched Failed");
        }


        // Display the Page Source Code
        String page_source = driver.getPageSource();
//        System.out.println(page_source);

        //close the browser
        driver.close();
    }
}
