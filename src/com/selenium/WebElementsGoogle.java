package com.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class WebElementsGoogle {
    public static void main(String[] args) {
        // open the Chrome browser
        System.setProperty("webdriver.chrome.browser", "/home/deexit/Downloads/Selenium/drivers");
        WebDriver driver = new ChromeDriver();

        // max the Chrome browser
        driver.manage().window().maximize();

        //Navigate to the URL
        driver.get("https://www.google.com");

        //[By TagName]
        //Here find-elements returns list type collections
        List<WebElement> link = driver.findElements(By.tagName("a"));
        System.out.println(link.size());

        //display the name of links
        for (WebElement l : link) {
            System.out.println(l.getText());
        }
    }
}
