package com.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Main {
    public static void main(String[] args) {
        //1. Open the Chrome Browser
        System.setProperty("webdrive.chrome.driver", "/home/deexit/Downloads/Selenium/drivers/chromedriver");
        WebDriver driver = new ChromeDriver();
        //2. Navigate to the url
        driver.get("https://www.google.com");
        //3. Close the browser
        driver.close();
    }
}